package com.service.account.advice;

import java.util.NoSuchElementException;

import javax.persistence.EntityNotFoundException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.service.account.exception.EmptyInputException;
import com.service.account.exception.InsufficientFundsException;
import com.service.account.exception.InvalidAmountException;



@ControllerAdvice
public class ExceptionControllerAdvice extends ResponseEntityExceptionHandler {

	
	@ExceptionHandler(NoSuchElementException.class)
	public ResponseEntity<String> handleNoSuchElementException()
	{
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body("No data found");
	}
	
	@ExceptionHandler(EntityNotFoundException.class)
	public ResponseEntity<String> handleEntityNotFoundException()
	{
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body("No data found");
	}
	
	@ExceptionHandler(EmptyInputException.class)
	public ResponseEntity<String> handleEmptyInputException(EmptyInputException emptyInputException)
	{
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(emptyInputException.getMessage());
	}	
	
	

	//@ExceptionHandler(HttpRequestMethodNotSupportedException.class)
	protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		// TODO Auto-generated method stub
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Invalid request type,Please change request method");
	}
	
	@ExceptionHandler(MethodArgumentTypeMismatchException.class)
	public ResponseEntity<String> handleMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException matme)
	{
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid request data, Please send correct data");
	}
	
	@ExceptionHandler(InsufficientFundsException.class)
	public ResponseEntity<String> handleInsufficientFundsException(InsufficientFundsException insufficientFundsException)
	{
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(insufficientFundsException.getMessage());
	}
	
	@ExceptionHandler(InvalidAmountException.class)
	public ResponseEntity<String> handleInvalidAmountException(InvalidAmountException invalidAmountException)
	{
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(invalidAmountException.getMessage());
	}	
	
	//@ExceptionHandler(HttpMessageNotReadableException.class)
	@Override
	protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		// TODO Auto-generated method stub
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Please provide the correct data");
	}
	


}
