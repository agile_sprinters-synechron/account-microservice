package com.service.account.config;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.github.javafaker.Faker;
import com.service.account.entities.Account;
import com.service.account.entities.Customer;
import com.service.account.entities.TransactionType;
import com.service.account.repository.AccountRepository;

@Component
public class BootstrapAppData {
	
//	  @Autowired private AccountRepository accountRepository; private final Faker
//	  faker = new Faker();
//	  
//	  @PostConstruct public void setUp() { for (int index = 0; index < 30; index++)
//	  { 
//		  Customer customer = Customer.builder()
//				  .fname(faker.name().firstName())
//				  .lname(faker.name().lastName())
//				  .email(faker.name().firstName() +"@gmail.com") 
//				  .creditScore(faker.number().numberBetween(800, 900))
//				  .mobileNo(faker.phoneNumber().cellPhone()).build();
//	  
//		  Account account = Account.builder() 
//				  			.amount(faker.number().randomDouble(2,200000, 500000))
//				  			.type(faker.options().option(TransactionType.class))
//				  			.customer(customer).build();
//	  
//	  this.accountRepository.save(account); } }
	 
	@Bean
	public RestTemplate getRestTemplate() {
		return new RestTemplate();
	}

}
