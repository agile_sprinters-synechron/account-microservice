package com.service.account.controller;

import java.util.ArrayList;
import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.service.account.dto.BalanceInfo;
import com.service.account.entities.Account;
import com.service.account.entities.Transaction;
import com.service.account.exception.InsufficientFundsException;
import com.service.account.service.AccountService;

@RestController
@RequestMapping("/api")
public class AccountController {

	@Autowired
	private AccountService accountService;
	


	@PostMapping("/accounts/save")
	public ResponseEntity<Account> createAccount(@RequestBody Account account) {
		Account saveAccount = accountService.saveAccount(account);
		// return ResponseEntity.ok(saveAccount);
		return ResponseEntity.status(HttpStatus.CREATED).body(saveAccount);
	}

	@GetMapping("/accounts/{id}")
	public List<BalanceInfo> getBalance(@PathVariable("id") int id) {
		BalanceInfo balanceInfo = accountService.getBalance(id);
		
		if(balanceInfo == null)
		{
			List<BalanceInfo> listBalInfo = new ArrayList<BalanceInfo>();
			return listBalInfo;
		}
		return List.of(balanceInfo);
		//return ResponseEntity.status(HttpStatus.OK).body(List.of(balanceInfo));
	}
	
	//added from loan service consumption
	@GetMapping("/account/{id}")
	public ResponseEntity<BalanceInfo> getBal(@PathVariable("id") int id) {
		BalanceInfo balanceInfo = accountService.getBalance(id);
		return ResponseEntity.status(HttpStatus.OK).body(balanceInfo);
	}

	@PostMapping("/accounts/{id}")
	public ResponseEntity<String> makeTransaction(@PathVariable("id") int id, @RequestBody Transaction transaction)
			throws InsufficientFundsException {
		Account finalAccount = accountService.makeTransaction(id, transaction.getType(), transaction.getAmount());
        
		if(finalAccount == null)
		{
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Account not available for this Account ID");
		}

		return ResponseEntity
				.ok(transaction.getType().name() + " of Rs: " + transaction.getAmount() + " is sucessfull ");
	}

}
