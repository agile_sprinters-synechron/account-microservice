package com.service.account.controller;


import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.service.account.entities.Statement;
import com.service.account.service.StatementService;



@RestController
@RequestMapping("/api")
public class StatementController {

	@Autowired
	private StatementService service;
	///api/accounts/{id}/statement
	@GetMapping("/accounts/{id}/statement")
	public ResponseEntity<List<Statement>> getStatementForAccountId(@PathVariable("id") int id)
	{
		List<Statement> statements = service.getStatements(id);
		
		return ResponseEntity.ok(statements);
	}

	
//	@GetMapping("/accounts/{id}/statement/{field}")
//	public ResponseEntity<List<Statement>> sortStatementsByField(@PathVariable("id") int id,@PathVariable("field") String field)
//	{
//		List<Statement> statementsByField = service.sortStatementsByField(id,field);
//		 
//		 return ResponseEntity.ok(statementsByField);
//	}
//	
//	@GetMapping("/accounts/{id}/statement/{from}/{to}")
//	 //public List<Statement> findByPagination(@PathVariable("from") LocalDateTime from,@PathVariable("to") LocalDateTime to,@PathVariable("id") int id)
//    public Page<List<Statement>> findByPagination(@PathVariable("from") @DateTimeFormat(pattern = "yyyy-MM-dd:HH-mm-ss")  LocalDateTime from,@PathVariable("to") @DateTimeFormat(pattern = "yyyy-MM-dd:HH-mm-ss")  LocalDateTime to,@PathVariable("id") int id,
//    		
//    	            @RequestParam(name = "page", required = false, defaultValue = "0") int page,
//    	            @RequestParam(name = "size", required = false, defaultValue = "10") int size,
//    	            @RequestParam(name = "attr", required = false, defaultValue = "amount") String property  		
//    		)  
//    {
//		
//		//LocalDateTime.of(id, id, id, id, id)
//		
//    	return service.findByPagination(from, to, id, page, size, property);
//    }
	
	

	
	//@GetMapping()
//	@GetMapping("/accounts/{id}/statement/date")	
//	public ResponseEntity<List<Statement>> getStatementsForIdSortedBy(@PathVariable("id") int id,
//			@RequestParam("fromDate") Date fromDate,
//			@RequestParam("toDate") Date toDate)
//			//@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date fromDate,
//			//@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date toDate)
//	{
//		System.out.println("FRom Date====="+fromDate+" To Date =========="+toDate);
//		List<Statement> statementsByField = service.getStatementsForIdSortedBy(id,fromDate,toDate);
//		return ResponseEntity.ok(statementsByField);;
//	}
	
	
	@GetMapping(path = "/accounts/{id}/statement/{startDate}/{endDate}")///customer/{customerId}/{startDate}/{endDate}
	public ResponseEntity<List<Statement>>  getStatementsWithPagination(@PathVariable int id,
			@PathVariable @DateTimeFormat(pattern = "dd-MM-yyyy HH:mm:ss") String startDate,
			@PathVariable @DateTimeFormat(pattern = "dd-MM-yyyy HH:mm:ss") String endDate,
			
            @RequestParam(name = "page", required = false, defaultValue = "0") int page,
            @RequestParam(name = "size", required = false, defaultValue = "10") int size,
            @RequestParam(name = "attr", required = false, defaultValue = "amount") String property, 
            @RequestParam(name = "order", required = false, defaultValue = "asc") String order
			)
	{
        
		String[] dateArr = startDate.split(" ");
		String month = dateArr[1];
		String date = dateArr[2];
		String year = dateArr[3];
		
		String time = dateArr[4];
		String[] timeArr = time.split(":");
		String hr = timeArr[0];
		String min = timeArr[1];
		String sec = timeArr[2];
		
		String strDate = year+"-"+month+"-"+date+" "+hr+":"+min+":"+sec;
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MMM-dd HH:mm:ss"); 
		LocalDateTime ldtStartDate = LocalDateTime.parse(strDate, formatter);
		
		String[] dateArr1 = endDate.split(" ");
		String month1 = dateArr1[1];
		String date1 = dateArr1[2];
		String year1 = dateArr1[3];
		
		String time1 = dateArr[4];
		String[] timeArr1 = time.split(":");
		String hr1 = timeArr1[0];
		String min1 = timeArr1[1];
		String sec1 = timeArr1[2];
		
		String strDate1 = year1+"-"+month1+"-"+date1+" "+hr1+":"+min1+":"+sec1;
		
		DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("yyyy-MMM-dd HH:mm:ss"); 
		LocalDateTime ldtEndDate = LocalDateTime.parse(strDate1, formatter1);


		
		
		System.out.println(startDate+" ============= "+ldtStartDate+" ============ "+ldtEndDate);
		
		Page<Statement> customer = service.getStatementsWithPagination(id, ldtStartDate, ldtEndDate, page, size, property, order);
		
		if(customer == null)
		{
			
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(List.of());
		}
		
		List<Statement> content = customer.getContent();
		return ResponseEntity.status(HttpStatus.OK).body(content);
		//return new ResponseEntity<>(content, HttpStatus.OK);//ResponseEntity<Page<Statement>>  ResponseEntity<List<Statement>> 
	}
	
}

