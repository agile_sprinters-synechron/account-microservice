package com.service.account.entities;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Builder;

@Entity
@Table(name = "account")
@Builder
public class Account {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int accountId;
	private double amount;
	// private String type;

	@Enumerated(EnumType.STRING)
	private TransactionType type;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "customer_id")
	private Customer customer;
	
	public Account() {
		// TODO Auto-generated constructor stub
	}

	public Account(int accountId, double amount, TransactionType type, Customer customer) {
		super();
		this.accountId = accountId;
		this.amount = amount;
		this.type = type;
		this.customer = customer;
	}

	public Account(double amount, TransactionType type, Customer customer) {
		super();
		// this.accountId = accountId;
		this.amount = amount;
		this.type = type;
		this.customer = customer;
	}

	public int getAccountId() {
		return accountId;
	}

	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public Customer getCustomer() {
		return customer;
	}

	public TransactionType getType() {
		return type;
	}

	public void setType(TransactionType type) {
		this.type = type;
	}

	public void setCustomer(Customer customer) {
		System.out.println("=========CUSTOMER==========" + customer);
		this.customer = customer;
	}
}
