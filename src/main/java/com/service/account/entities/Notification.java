package com.service.account.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


public class Notification {

	private Integer customerid;
	private String email_address;
	private String message;
	private String phone;

	public Notification() {
	}

	public Notification(Integer customerid, String email_address, String message, String phone) {
		this.customerid = customerid;
		this.email_address = email_address;
		this.message = message;
		this.phone = phone;
	}


	public Integer getCustomerid() {
		return customerid;
	}

	public void setCustomerid(Integer customerid) {
		this.customerid = customerid;
	}

	public String getEmail_address() {
		return email_address;
	}

	public void setEmail_address(String email_address) {
		this.email_address = email_address;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Override
	public String toString() {
		return "Notification [customerid=" + customerid + ", email_address=" + email_address
				+ ", message=" + message + ", phone=" + phone + "]";
	}


	
	
}
