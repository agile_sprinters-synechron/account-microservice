package com.service.account.entities;


import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.RequestParam;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;


@Entity
@Table(name = "statement")
public class Statement {

	@Id
	@GeneratedValue
	private int sid;
	private String transactionType;
	private double amount;
	
	
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonProperty(value = "time", required = true)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
   
    @DateTimeFormat(pattern = "yyyy-MM-dd:HH-mm-ss")     
	private LocalDateTime time;
	
	
	private int accountId;
	/*
	 * @OneToOne(mappedBy = "statement") private Account account;
	 */
	
	public Statement() {
		// TODO Auto-generated constructor stub
	}
	
	

	public Statement(String transactionType, double amount, LocalDateTime time, int accountId) {
		super();
		//this.sid = sid;
		this.transactionType = transactionType;
		this.amount = amount;
		this.time = time;
		this.accountId = accountId;
	}




	public int getSid() {
		return sid;
	}

	public void setSid(int sid) {
		this.sid = sid;
	}


	public String getTransactionType() {
		return transactionType;
	}



	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}



	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}
	
    
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")   
	public LocalDateTime  getTime() {
		return time;
	}

	public void setTime(LocalDateTime time) {
		this.time = time;
	}



	public int getAccountId() {
		return accountId;
	}



	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}



	public Object getField(String field) {
		// TODO Auto-generated method stub
		
		if(field.equals("accountId"))
		   return getAccountId();
		else if(field.equals("sid"))
			   return getSid();
		else if(field.equals("transaction_type"))
			   return getTransactionType();
		else if(field.equals("amount"))
			   return getAmount();
		else
			   return getTime();		
	}

	
}
