package com.service.account.entities;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

public class Transaction {

	// private int accountId;
	private double amount;

	@Enumerated(EnumType.STRING)
	private TransactionType type;

	public Transaction() {
		// TODO Auto-generated constructor stub
	}

	public Transaction(double amount) {
		super();
		// this.accountId = accountId;
		this.amount = amount;
	}

	public Transaction(double amount, TransactionType type) {
		super();
		this.amount = amount;
		this.type = type;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public TransactionType getType() {
		return type;
	}

	public void setType(TransactionType type) {
		this.type = type;
	}

}
