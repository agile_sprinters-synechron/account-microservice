package com.service.account.entities;

public enum TransactionType {
	
	DEBIT,
	CREDIT

}
