package com.service.account.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.service.account.entities.Account;

@Repository
public interface AccountRepository extends JpaRepository<Account, Integer> {

}
