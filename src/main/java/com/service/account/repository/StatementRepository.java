package com.service.account.repository;

import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.service.account.entities.Statement;

@Repository
public interface StatementRepository extends JpaRepository<Statement, Integer> {

	public List<Statement> findByAccountId(int id);

	@Query("select u from Statement u where u.accountId = : accountId")
	List<Statement> findByAndSort(@Param("accountId") int aid, Sort sort);

	public List<Statement> findByTimeBetween(LocalDateTime startdate, LocalDateTime endDate);

	@Query(value = "SELECT * FROM sys.statement where account_id = ?1 and time between ?2 and ?3", nativeQuery = true)
	Page<List<Statement>> findByAccountIdAndDateBetween(int accountId, LocalDateTime from, LocalDateTime to,
			Pageable pageable);

	public Page<Statement> findAllByAccountIdAndTimeBetween(int accountId, LocalDateTime from, LocalDateTime to,
			Pageable pageable);

}
