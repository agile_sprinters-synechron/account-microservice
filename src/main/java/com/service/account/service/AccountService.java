package com.service.account.service;


import java.time.LocalDateTime;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.service.account.dto.BalanceInfo;
import com.service.account.entities.Account;
import com.service.account.entities.Customer;
import com.service.account.entities.Notification;
import com.service.account.entities.Statement;
import com.service.account.entities.TransactionType;
import com.service.account.exception.EmptyInputException;
import com.service.account.exception.InsufficientFundsException;
import com.service.account.exception.InvalidAmountException;
import com.service.account.repository.AccountRepository;
import com.service.account.repository.StatementRepository;


@Service
public class AccountService {
	
	@Autowired
	private AccountRepository accountRepository;
	
	@Autowired
	private StatementRepository statementRepository;
	
	@Autowired
	private RestTemplate restTemplate;
	
	private final String NOTIFICATION_API = "http://104.198.54.20:8080/api/notifications/customer/";


	public Account saveAccount(Account account) {
		checkforEmptyInput(account);		
		return accountRepository.save(account);
	}

	public BalanceInfo getBalance(int id) {
		Optional<Account> accountOptional = getAccountById(id);//accountRepository.getById(id);
		Account account = null;
		BalanceInfo balanceInfo;
		if(!accountOptional.isPresent())
			balanceInfo = null;
		else
		{
			account = accountOptional.get();
		
		
		int accId = account.getAccountId();
		int customerId = account.getCustomer().getId();
		String email = account.getCustomer().getEmail();
		String mobileNo = account.getCustomer().getMobileNo();
		int creditScore = account.getCustomer().getCreditScore();
		String name = account.getCustomer().getFname()+" "+account.getCustomer().getLname();
		double bal = account.getAmount();
		
		 balanceInfo = new BalanceInfo(accId, customerId, creditScore, name, bal, email, mobileNo);
	   }
		 return balanceInfo;
	}

	public Optional<Account> getAccountById(int id) {		
		Optional<Account> account = Optional.ofNullable(accountRepository.findById(id)).get();//accountRepository.findById(id)
		return account;
	}

	
	@Transactional
	public Account makeTransaction(int id,TransactionType type, double amount) {
		 Optional<Account> accountOptional = getAccountById(id);
		 Account account = null;
		if(!accountOptional.isPresent())
				return null;
		else
			account = accountOptional.get();
		 
		 double balance = 0.0;
		 String msg = null;
			
		if(type.toString().equals("DEBIT")) 
		{
			if(account.getAmount() < amount)
			{
			   	throw new InsufficientFundsException("Insufficient Balance, Cannot debit amount");
			}
			else if(amount <= 0)
			{
			   	throw new InvalidAmountException("Please provide valid amount to be dedited");
			}
			else
			{		 
			    balance = account.getAmount()-amount;	
			    msg = "Hello, "+account.getCustomer().getFname()+"\r\n  Amount of Rs. "+amount+" has been debited successfully at "+LocalDateTime.now().toString()+".\r\n Happy Banking";
			}
		}
		if(type.toString().equals("CREDIT")) 
		{
			if(amount <= 0)
			{
			   	throw new InvalidAmountException("Please provide valid amount to be credited");
			}
			else
			{		 
			    balance = account.getAmount()+amount;			
			    msg = "Hello, "+account.getCustomer().getFname()+"\r\n  Amount of Rs. "+amount+" has been credited successfully at "+LocalDateTime.now().toString()+".\r\n Happy Banking";
			}			
		}
		
		 account.setAmount(balance);
		 Account saveAccount = saveAccount(account);
		 
		 LocalDateTime date = LocalDateTime.now();
		 Statement statement = new Statement(type.toString(), amount,date,account.getAccountId());
		 statementRepository.save(statement);
		 
		 Customer customer = account.getCustomer();
		 
//	 Notification notification = 
//			 new Notification(customer.getId(), customer.getEmail(), msg, customer.getMobileNo());
		 
	 Notification notification = 
			 new Notification(customer.getId(),"mansaremanish10@gmail.com", msg, "+917875771830");		 
	 
		 ResponseEntity<Notification> notificationResponse = restTemplate.postForEntity(NOTIFICATION_API+customer.getId(), notification, Notification.class);
		 System.out.println(notificationResponse.getBody());
		 
		 return saveAccount;
	}
	
	public static void checkforEmptyInput(Account account)
	{
		if(account.getAmount() == 0.0)
			throw new EmptyInputException("Please enter valid amount");
		if(account.getType() == null || account.getType().name() == "")
			throw new EmptyInputException("Please enter valid account type");
		if(account.getCustomer().getFname() == "" || account.getCustomer().getFname() == null)
			throw new EmptyInputException("First Name is missing, it is mandatory.");
		if(account.getCustomer().getLname() == "" || account.getCustomer().getFname() == null)
			throw new EmptyInputException("Last Name is missing, it is mandatory");
		if(account.getCustomer().getEmail() == "" || account.getCustomer().getEmail() == null)
			throw new EmptyInputException("Email is missing, it is mandatory");
		if(account.getCustomer().getMobileNo() == "" || account.getCustomer().getMobileNo() == null)
			throw new EmptyInputException("Mobile No is missing, it is mandatory");
	}


}
