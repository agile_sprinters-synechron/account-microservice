package com.service.account.service;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.service.account.entities.Account;
import com.service.account.entities.Statement;
import com.service.account.repository.AccountRepository;
import com.service.account.repository.StatementRepository;

@Service
public class StatementService {

	@Autowired
	private StatementRepository repository;
	
	@Autowired
	private AccountRepository accountRepository;

	public List<Statement> getStatements(int id) {
		// TODO Auto-generated method stub
		List<Statement> statemetsList = repository.findByAccountId(id);

		if (statemetsList.size() == 0)
			throw new EntityNotFoundException();

		return statemetsList;
	}

	public List<Statement> sortStatementsByField(int accountId, String field) {

		Sort sort = Sort.by(field).ascending();
		List<Statement> listStatements = repository.findByAndSort(accountId, sort);
		return listStatements;
	}

	public List<Statement> getStatementsForIdSortedBy(int id, LocalDateTime startdate, LocalDateTime endDate) {
		List<Statement> findByTimeBetween = repository.findByTimeBetween(startdate, endDate);
		return findByTimeBetween;
	}

//	public Page<List<Statement>> findByPagination(LocalDateTime startdate, LocalDateTime endDate, int id, int page,
//			int size, String property) {
//
//		PageRequest pageRequest = PageRequest.of(page, size, Sort.by(property));
//		final Page<List<Statement>> pageResponse = this.repository.findByAccountIdAndDateBetween(id, startdate, endDate,
//				pageRequest);
//		return pageResponse;
//	}

	public Page<Statement> getStatementsWithPagination(int customerId, LocalDateTime startDate, LocalDateTime endDate, int page,
			int size, String property, String order) {
		
		Optional<Account> accountOptional = getAccountById(customerId);//accountRepository.getById(id);
		Account account = null;
		
		if(!accountOptional.isPresent())
			return null;

		PageRequest pageRequest = null;
		if (order.equalsIgnoreCase("desc"))
			pageRequest = PageRequest.of(page, size, Sort.by(Direction.DESC, property));
		else
			pageRequest = PageRequest.of(page, size, Sort.by(property));
		return repository.findAllByAccountIdAndTimeBetween(customerId, startDate, endDate, pageRequest);
	}
	
	public Optional<Account> getAccountById(int id) {		
		Optional<Account> account = Optional.ofNullable(accountRepository.findById(id)).get();//accountRepository.findById(id)
		return account;
	}

}
